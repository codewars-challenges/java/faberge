package be.hics.sandbox.faberge;

import java.math.BigInteger;
import java.util.stream.LongStream;

/**
 * https://www.codewars.com/kata/faberge-easter-eggs-crush-test
 * Kata
 * <p>
 * One man (lets call him Eulampy) has a collection of some almost identical Fabergè eggs. One day his friend Tempter said to him:
 * <ul>
 * <li>Do you see that skyscraper? And can you tell me a maximal floor that if you drop your egg from will not crack it?</li>
 * <li>No, - said Eulampy.</li>
 * <li>But if you give me N eggs, - says Tempter - I'l tell you an answer.</li>
 * <li>Deal - said Eulampy. But I have one requirement before we start this: if I will see more than M falls of egg, my heart will be crushed instead of egg. So you have only M trys to throw eggs. Would you tell me an exact floor with this limitation?</li>
 * </ul>
 * </p>
 * <p>
 * Task
 * Your task is to help Tempter - write a function
 * <div>
 * height :: Integer -> Integer -> Integer
 * height n m = -- see text
 * </div>
 * </p>
 * that takes 2 arguments - the number of eggs n and the number of trys m - you should calculate maximum scyscrapper height (in floors), in which it is guaranteed to find an exactly maximal floor from which that an egg won't crack it.
 * <p>
 * Which means,
 * <ol>
 * <li>You can throw an egg from a specific floor every try</li>
 * <li>Every egg has the same, certain durability - if they're thrown from a certain floor or below, they won't crack. Otherwise they crack</li>
 * <li>You have n eggs and m tries</li>
 * <li>What is the maxmimum height, such that you can always determine which floor the target floor is when the target floor can be any floor between 1 to this maximum height?</li>
 * </ol>
 * </p>
 * <p>
 * Examples
 * <div>
 * height 0 14 = 0
 * height 2 0  = 0
 * height 2 14 = 105
 * height 7 20 = 137979
 * </div>
 * </p>
 * <p>
 * Data range
 * <div>
 * n <= 20000
 * m <= 20000
 * </div>
 * </p>
 */
public class Faberge {

    public static BigInteger height(BigInteger eggs, BigInteger floor) {
        if (eggs.compareTo(BigInteger.ZERO) == 0 || floor.compareTo(BigInteger.ZERO) == 0)
            return BigInteger.ZERO;
        return calculateBase(floor).subtract(calculateSmartSumOfDeltas(eggs, floor));
    }

    private static BigInteger calculateBase(BigInteger floor) {
        return BigInteger.valueOf(2).pow(floor.intValue()).subtract(BigInteger.ONE);
    }

    private static BigInteger calculateSmartSumOfDeltas(BigInteger eggs, BigInteger floor) {
        long diffBetweenFloorsAndEggs = floor.subtract(eggs).longValue();
        return LongStream.rangeClosed(0, diffBetweenFloorsAndEggs - 1).parallel().boxed().map(i -> facDiv(floor.longValue(), i)).reduce(BigInteger.ZERO, BigInteger::add);
    }

    private static BigInteger factorial(Long n) {
        if (n == 0)
            return BigInteger.ONE;
        return LongStream.rangeClosed(2, n).parallel().mapToObj(BigInteger::valueOf).reduce(BigInteger.ONE, BigInteger::multiply);
    }

    private static BigInteger facDiv(Long n, Long i) {
        if (n == 0 || i == 0)
            return BigInteger.ONE;
        BigInteger f_fac_divide_f_i_fac = LongStream.rangeClosed(n - i + 1L, n).parallel().mapToObj(BigInteger::valueOf).reduce(BigInteger.ONE, BigInteger::multiply);
        return f_fac_divide_f_i_fac.divide(factorial(i));
    }

}
