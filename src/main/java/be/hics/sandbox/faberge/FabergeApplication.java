package be.hics.sandbox.faberge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StopWatch;

import java.math.BigInteger;
import java.util.Arrays;

@SpringBootApplication
public class FabergeApplication {

	public static void main(String[] args) {
		SpringApplication.run(FabergeApplication.class, args);
        StopWatch stopWatch = new StopWatch();
        int[][] ints = new int[][] {
                {10, 10}, {9, 10}, {8, 10}, {7, 10}, {6, 10}, {5, 10}, {4, 10}, {3, 10}, {2, 10}, {1, 10},
                {1000, 1000}, {900, 1000}, {800, 1000}, {700, 1000}, {600, 1000}, {500, 1000}, {400, 1000}, {300, 1000}, {200, 1000}, {100, 1000},
                {20000, 20000}, {19000, 20000}, {18000, 20000}
        };
        Arrays.stream(ints).forEach(i -> heightWithPrettyPrint(stopWatch, BigInteger.valueOf(i[0]), BigInteger.valueOf(i[1])));
        System.out.println(stopWatch.prettyPrint());
	}

	private static BigInteger heightWithPrettyPrint(StopWatch stopWatch, BigInteger eggs, BigInteger floor) {
		stopWatch.start(String.format("heightWithPrettyPrint(%s, %s)", eggs.toString(), floor.toString()));
		BigInteger calculatedHeight = Faberge.height(eggs, floor);
		System.out.println(String.format("Faberge.heightWithPrettyPrint(%s, %s) => %s", eggs.toString(), floor.toString(), calculatedHeight.toString()));
		stopWatch.stop();
		return calculatedHeight;
	}

}
